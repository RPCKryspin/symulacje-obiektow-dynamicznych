clear all;
close all ;

g = 9.81;
A1 = 5*5;
A2 = 5*10;
Aw1 = 2*1;
Aw2 = 2*2;

timestep = 50;
dfwe = 1;
dfwe2 = 0;
timesim=200.0;


fwemax = 10;
fwe2max = 5;
fwe2 = 1;
identify_fwe = [0.3 0.6 0.9];

h2max = ((1/Aw2)^2/(2*g))*(fwemax+fwe2max)^2;
h1max = ((1/Aw1)^2/(2*g))*(fwemax)^2 + h2max;
a1 = Aw1*sqrt((2*g)/(h1max-h2max));
a2 = Aw2*sqrt((2*g)/h2max);

M = [A1*A2, a1*A1+a2*A1+a1*A2, a1*a2];
M11 = [A2, a1+a2];
M12 = a1;
M21 = a1;
M22 = [A1, a1];

A = [-a1/A1,a1/A1;a1/A2,-(a1+a2)/A2];
B = [1/A1,0;0,1/A2];
C = [1,0;0,1];
D = [0,0;0,0];

znacznik = ["k-", "m--", "c."];

for i = 1:3

    fwe = identify_fwe(i)*fwemax;

    h20 = (( 1/Aw2)^2/( 2*g))* (fwe+fwe2)^2;
    h10 = (( 1/Aw1)^2/( 2*g))* (fwe)^2  +h20 ;

    linh20 = (fwe+fwe2)/a2;
    linh10 = fwe/a1 + linh20;

    u=[fwe;fwe2];
    linh = -A^(-1)*B*u;

    sim('Kaskada_wodna_schemat');
    
 figure(1);
    subplot(221);
    hold on;
    plot(ans.tout, ans.h1, znacznik(i));
    subplot(222);
    hold on;
    plot(ans.tout, ans.h2, znacznik(i));
    subplot(223);
    hold on;
    plot(ans.tout, ans.h1-h10, znacznik(i));
    subplot(224);
    hold on;
    plot(ans.tout, ans.h2-h20, znacznik(i));

figure(2)
    subplot(221);
    hold on;
    plot(ans.tout, ans.linh1, znacznik(i));
    subplot(222);
    hold on;
    plot(ans.tout, ans.linh2, znacznik(i));
    subplot(223);
    hold on;
    plot(ans.tout, ans.linh1-linh10, znacznik(i));
    subplot(224);
    hold on;
    plot(ans.tout, ans.linh2-linh20, znacznik(i));

 figure (3)
    subplot(121);
    hold on;
    plot(ans.tout, ans.h1, 'k' );
    plot(ans.tout, ans.linh1, 'm');
    subplot(122);
    hold on;
    plot(ans.tout, ans.h1-h10, 'k');
    plot(ans.tout, ans.linh1-linh10, 'm');

    figure(4)
    subplot(221);
    hold on;
    plot(ans.tout, ans.hs1, znacznik(i));
    subplot(222);
    hold on;
    plot(ans.tout, ans.hs2, znacznik(i));
    subplot(223);
    hold on;
    plot(ans.tout, ans.hs1-linh(1), znacznik(i));
    subplot(224);
    hold on;
    plot(ans.tout, ans.hs2-linh(2), znacznik(i));

    figure(5)
    subplot(221);
    hold on;
    plot(ans.tout, ans.ht1, znacznik(i));
    subplot(222);
    hold on;
    plot(ans.tout, ans.ht2, znacznik(i));
    subplot(223);
    hold on;
    plot(ans.tout, ans.ht1-linh(1),znacznik(i));
    subplot(224);
    hold on;
    plot(ans.tout, ans.ht2-linh(2), znacznik(i));
end

figure(1);
subplot(221);
grid on;
title('Wysokość h1 nieliniowa');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(222);
grid on;
title('Wysokość h2 nieliniowa');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(223);
grid on;
title('zmiana h1 nieliniowa');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(224);
grid on;
title('zmiana h2 nieliniowa');
xlabel('Czas [s]');
ylabel('Wysokość');

figure(2);
subplot(221);
grid on;
title('Wysokość h1 liniowa');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(222);
grid on;
title('Wysokość h2 liniowa');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(223);
grid on;
title('zmiana h1 liniowy');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(224);
grid on;
title('zmiana h2 liniowy');
xlabel('Czas [s]');
ylabel('Wysokość');

figure(3);
subplot(121);
grid on;
title('Porównanie wysokości h1  lin. i nielin.');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(122);
grid on;
title('Porównanie przyrostu h1  lin. i nielin.');
xlabel('Czas [s]');
ylabel('Wysokość');

figure(4);
subplot(221);
grid on;
title('Wysokość h1 state-space');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(222);
grid on;
title('Wysokość h2 state-space');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(223);
grid on;
title('zmiana h1 state-space');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(224);
grid on;
title('zmiana h2 state-space');
xlabel('Czas [s]');
ylabel('Wysokość');

figure(5);
subplot(221);
grid on;
title('Wysokość h1  transmintancja');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(222);
grid on;
title('Wysokość h2 transmintancja');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(223);
grid on;
title('Zmiana h1 transmintancja');
xlabel('Czas [s]');
ylabel('Wysokość');
subplot(224);
grid on;
title('Zmiana h2 transmintancja ');
xlabel('Czas [s]');
ylabel('Wysokość');



