clear all ;
close all;

%---Parmatery Nominalne Wejsciowe i statyczne
P_gN = 15000;       %[W] Moc grzejnika
T_zewN = -20;       %[C] Temperatura na zewnatrz
T_wewN = 20;        %[C] Temperatura wewnątrz
T_sN = 15;          %[C] Temperatura ścian
V_o = 2.5*15*6.5 ;  %[m3] Objetosc obiektu domowego
V_w = 2.5*12*5;     %[m3] Objętośc pokoju (150 m3)
V_s = V_o-V_w;      %[m3] Objętość scian (94 m3)
F_p = V_w/(30*60);  %[m3/s] wietrzenie pokoju (30 min)
Cw_p = 1000;        %[J/Kg*C] Ciepło Właściwe powietrza             
Cw_m = 840;         %[J/Kg*C] Ciepło Właściwe betona 
D_p = 1.2;          %[Kg/m3] Gęstość powietrza            
D_m = 800;          %[Kg/m3] Gęstość betona

%---Identyfikacja parametrów
K_w = (P_gN+Cw_p*D_p*F_p*(T_zewN-T_wewN))/(T_wewN-T_sN);
K_s = (P_gN+Cw_p*D_p*F_p*(T_zewN-T_wewN))/(T_sN-T_zewN);

%---Parametry dynamiczne 
C_vs = Cw_m*D_m*V_s;    % [W/C] Pojemnosc cieplna sciany
C_vw = Cw_p*D_p*V_w;    % [W/C] Pojemnosc cieplna pokoju

%--------------------------Model Nieliniowy
for i=1:3   
zmiana_Tz=[10,20,30];
zmiana_Pg=[0.8,0.6,0.4];
zmiana_Fp=[1.5,1.0,0.5];
kolor=['k--','r--','g--'];

%-------------------------- Warunki początkowe
T_zew0 = T_zewN +  zmiana_Tz(i);  % Zmiany Temperatury na zewnątrz
P_g0 = P_gN *zmiana_Pg(i) ;    % pokrętło Mocy grzejnika
F_p0 = F_p *  zmiana_Fp(i) ;    % częstotliwość wymiany powietrza 

%-------------------------- Punkty równowagi i stan ustalony
T_wew0 = ((K_s+K_w)*(P_g0 + Cw_p*D_p*F_p0*T_zew0) +K_s*K_w*T_zew0)/((K_w+Cw_p*D_p*F_p0)*(K_s+K_w)-K_w*K_w);
T_s0 = (K_w*T_wew0+K_s*T_zew0)/(K_s+K_w);


%---------------------------- Parametry symulacyjne i modele MIMO
t_step = 50000;
T_sim = 1000000;
dT = 0;
dP = 0.1*P_gN;
dF = 0;
sim('Model_cieplny_schemat');

%-------------symulacje nieliniowe
figure(2);
subplot(2,1,1);
hold on;
grid on;
plot(ans.tout, ans.T_wew,kolor(i));
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_{wew}');
subplot(2,1,2);
hold on;
grid on;
plot(ans.tout, ans.T_s, kolor(i));
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_s');
end


%------Modele Liniowe
%-------------------------Przepływ 1 *Fp
for i = 5:6

%---Warunki początkowe
zmiana_Tz=[0,40];
zmiana_Pg=[1,0.5];
kolor=["r.","b"];

T_zew0 = T_zewN +zmiana_Tz(i-4);  % Zmiany Temperatury na zewnątrz
P_g0 = P_gN *zmiana_Pg(i-4) ;     % pokrętło Mocy grzejnika
F_p0 = F_p * 0 ;                  % częstotliwość wymiany powietrza

%---Równanie stanu Linearyzacja Fp(t) --> Fp
A = [(-K_w-Cw_p*D_p*F_p0)/C_vw, K_w/C_vw; K_w/C_vs, (-K_w-K_s)/C_vs];
B = [Cw_p*D_p*F_p0/C_vw, 1/C_vw; K_s/C_vs, 0];
C = [1,0;0,1];
D = [0,0;0,0];

%---Transmintancja Linearyzacja Fp(t) --> Fp
s = tf('s');
M = C_vw*C_vs*s^2 + (C_vw*(K_w+K_s)+C_vs*(K_w+Cw_p*D_p*F_p0))*s + (K_w+Cw_p*D_p*F_p0)*(K_w+K_s)-K_w*K_w;
M11 = Cw_p*D_p*F_p0*C_vs*s + Cw_p*D_p*F_p0*(K_w+K_s)+K_w*K_s;
M12 = C_vs*s + K_w + K_s;
M21 = -K_s*C_vw*s + K_s*(K_w + Cw_p*D_p*F_p0)+K_w*Cw_p*D_p*F_p0;
M22 = K_w;

%---parametry funkcyjne
du = [0, 0.1*P_gN];
[ys, t1] = step(ss(A, B, C, D));
[ytwew, t2] = step(tf(M12/M)*du(2)+tf(M11/M)*du(1));
[ytg, t3] = step(tf(M22/M)*du(2)+tf(M21/M)*du(1));

%---symulacje liniowe
figure(i)
subplot(2,1,1);
hold on;
grid on;
plot(t1,ys(:,1,1)*du(1)+ys(:,1,2)*du(2), kolor(i-4));
plot(t2, ytwew, kolor(i-4)); %Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_{wew}');
subplot(2,1,2);
hold on;
grid on;
plot(t1, ys(:,2,1)*du(1)+ys(:,2,2)*du(2), kolor(i-4));
plot(t3, ytg, kolor(i-4)); %Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_s');

figure(9)
subplot(2,1,1);
hold on;
grid on;
plot(t1, ys(:,1,1)*du(1)+ys(:,1,2)*du(2), kolor(i-4));
plot(t2, ytwew, kolor(i-4)); %Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_{wew}');
subplot(2,1,2);
hold on;
grid on;
plot(t1, ys(:,2,1)*du(1)+ys(:,2,2)*du(2), kolor(i-4));
plot(t3, ytg, kolor(i-4)); %Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_s');

end
%---------------------------------------------przepływ 0.5Fp
for i = 7:8

%---Warunki początkowe
zmiana_Tz=[0,40];
zmiana_Pg=[1,0.5];
kolor=["k.","g"];

T_zew0 = T_zewN +zmiana_Tz(i-6);  % Zmiany Temperatury na zewnątrz
P_g0 = P_gN *zmiana_Pg(i-6) ;     % pokrętło Mocy grzejnika
F_p0 = F_p * 0.5 ;                % częstotliwość wymiany powietrza

%---Równanie stanu Linearyzacja Fp(t) --> Fp
A = [(-K_w-Cw_p*D_p*F_p0)/C_vw, K_w/C_vw; K_w/C_vs, (-K_w-K_s)/C_vs];
B = [Cw_p*D_p*F_p0/C_vw, 1/C_vw; K_s/C_vs, 0];
C = [1,0;0,1];
D = [0,0;0,0];

%---Transmintancja Linearyzacja Fp(t) --> Fp
s = tf('s');
M = C_vw*C_vs*s^2 + (C_vw*(K_w+K_s)+C_vs*(K_w+Cw_p*D_p*F_p0))*s + (K_w+Cw_p*D_p*F_p0)*(K_w+K_s)-K_w*K_w;
M11 = Cw_p*D_p*F_p0*C_vs*s + Cw_p*D_p*F_p0*(K_w+K_s)+K_w*K_s;
M12 = C_vs*s + K_w + K_s;
M21 = -K_s*C_vw*s + K_s*(K_w + Cw_p*D_p*F_p0)+K_w*Cw_p*D_p*F_p0;
M22 = K_w;

%---parametry funkcyjne 
du = [0, 0.1*P_gN];
[ys, t1] = step(ss(A, B, C, D));
[ytwew, t2] = step(tf(M12/M)*du(2)+tf(M11/M)*du(1));
[ytg, t3] = step(tf(M22/M)*du(2)+tf(M21/M)*du(1));

%---symulacje liniowe
figure(i)
subplot(2,1,1);
hold on;
grid on;
plot(t1,ys(:,1,1)*du(1)+ys(:,1,2)*du(2), kolor(i-6));
plot(t2, ytwew, kolor(i-6)); %Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_{wew}');
subplot(2,1,2);
hold on;
grid on;
plot(t1, ys(:,2,1)*du(1)+ys(:,2,2)*du(2), kolor(i-6));
plot(t3, ytg, kolor(i-6));%Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_s');

figure(9)
subplot(2,1,1);
hold on;
grid on;
plot(t1, ys(:,1,1)*du(1)+ys(:,1,2)*du(2), kolor(i-6));
plot(t2, ytwew, kolor(i-6));%Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_{wew}');
subplot(2,1,2);
hold on;
grid on;
plot(t1, ys(:,2,1)*du(1)+ys(:,2,2)*du(2), kolor(i-6));
plot(t3, ytg, kolor(i-6));%Nakładająca się krzywa
ylabel('Temperatura [^oC]');
xlabel('Czas [s]');
title('T_s');

end

%---Charakterystyki statyczne
P_g0 = P_gN;  
F_p0 = F_p ;    
T_zew0 = T_zewN-10:1:T_zewN+10;
T_wew0 = ((K_s+K_w)*(P_g0 + Cw_p*D_p*F_p0*T_zew0) +K_s*K_w*T_zew0)/((K_w+Cw_p*D_p*F_p0)*(K_s+K_w)-K_w*K_w);
T_s0 = (K_w*T_wew0+K_s*T_zew0)/(K_s+K_w);
figure(10)
subplot(2,1,1);
hold on;
grid on;
plot(T_zew0,T_wew0,'b--');
plot(T_zewN,T_wewN,'ro');
title('T_{wew}(Tzew)');
subplot(2,1,2);
hold on;
grid on;
plot(T_zew0,T_s0,'b--');
plot(T_zewN,T_sN,'ro');
title('T_s(Tzew)');
%---------------------------------------------Pg
F_p0 = F_p ;    
T_zew0 = T_zewN;
P_g0=[0:0.1*P_gN:P_gN+4000];
T_wew0 = ((K_s+K_w)*(P_g0 + Cw_p*D_p*F_p0*T_zew0) +K_s*K_w*T_zew0)/((K_w+Cw_p*D_p*F_p0)*(K_s+K_w)-K_w*K_w);
T_s0 = (K_w*T_wew0+K_s*T_zew0)/(K_s+K_w);
figure(11)
subplot(2,1,1);
hold on;
grid on;
plot(P_g0,T_wew0,'b--');
plot(P_gN,T_wewN,'ro');
title('T_{wew}(Pg)');
subplot(2,1,2);
hold on;
grid on;
plot(P_g0,T_s0,'b--');
plot(P_gN,T_sN,'ro');
title('T_s(Pg)');
%----------------------------------------------Fp
P_g0 = P_gN;  
T_zew0 = T_zewN;
F_p0=[0:0.1*F_p:F_p+0.8];
T_wew0 = ((K_s+K_w)*(P_g0 + Cw_p*D_p*F_p0*T_zew0) +K_s*K_w*T_zew0)./((K_w+Cw_p*D_p*F_p0)*(K_s+K_w)-K_w*K_w);
T_s0 = (K_w*T_wew0+K_s*T_zew0)/(K_s+K_w);
figure(12)
subplot(2,1,1);
hold on;
grid on;
title('T_{wew}(Fp)');
plot(F_p0,T_wew0,'b--');
plot(F_p,T_wewN,'ro');
subplot(2,1,2);
hold on;
grid on;
plot(F_p0,T_s0,'b--');
plot(F_p,T_sN,'ro');
title('T_s(Fp)');
