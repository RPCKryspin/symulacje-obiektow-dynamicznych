clear all;
close all;

%---Parametry równania
a=2; 
b=7; 
c=3;
t=[0:0.1:10];

%--- Analityczne rozwiązania
A=2/5;
B=-1/15;
Xs1=A*exp(-1/2*t);
Xs2=B*exp(-3*t);
XS=Xs1+Xs2;
Xw=t;
Xw(:)=(5/3);
X=XS+Xw;

%---Parametry symulacyje 
u0=5;
x0=2; 
x10=0; 
du=0;
tsim=10;
tstep=0; 
sim('Rownanie_rozniczkowe_schemat');

%---symulacja
figure(1)
grid on
hold on
plot(ans.tout,ans.x, 'co');
plot(t,Xs1, 'g')
plot(t,Xs2, 'b')
plot(t,XS,'k')
plot(t,Xw, 'r')
plot(t,X,'m')
ylabel('x');
xlabel('t');
legend('Sim','Xs1','Xs2','XS','Xw','X');
title('Prezentacja rozwiązania Równania Różniczkowego 1');

%----------------------------------------------------
A=0;
B=0;
Xs1=A*exp(-1/2*t);
Xs2=B*exp(-3*t);
XS=Xs1+Xs2;
Xw=t;
Xw(:)=(5/3);
X=XS+Xw;

u0=5;
x0=5/3; 
du=0;
sim('Rownanie_rozniczkowe_schemat');

figure(2)
grid on
hold on
plot(ans.tout,ans.x, 'co');
plot(t,Xs1, 'g')
plot(t,Xs2, 'b')
plot(t,XS,'k')
plot(t,Xw, 'r')
plot(t,X,'m')
ylabel('x');
xlabel('t');
legend('Sim','Xs1','Xs2','XS','Xw','X');
title('Prezentacja rozwiązania Równania Różniczkowego 2');

%-----------------------------------------------skok
A=-2;
B=1/3;
Xs1=A*exp(-1/2*t);
Xs2=B*exp(-3*t);
XS=Xs1+Xs2;
Xw=t;
Xw(:)=(5/3);
X=XS+Xw;

u0=0;
x0=0; 
du=5;
sim('Rownanie_rozniczkowe_schemat');

figure(3)
grid on
hold on
plot(ans.tout,ans.x, 'co');
plot(t,Xs1, 'g')
plot(t,Xs2, 'b')
plot(t,XS,'k')
plot(t,Xw, 'r')
plot(t,X,'m')
ylabel('x');
xlabel('t');
legend('Sim','Xs1','Xs2','XS','Xw','X');
title('Rozwiązanie skokowe Równania Różniczkowego');

%----------------------------------------------------impuls
A=1;
B=-1;
Xs1=A*exp(-1/2*t);
Xs2=B*exp(-3*t);
X=Xs1+Xs2;

u0=50;
x0=0; 
x10=0; 
du=-50;
tstep=0.1; 
sim('Rownanie_rozniczkowe_schemat');

figure(4)
grid on
hold on
plot(ans.tout,ans.x, 'co');
plot(t,X,'m')
ylabel('x');
xlabel('t');
legend('Sim','X');
title('Rozwiązanie impulsowe Równania Różniczkowego ');


