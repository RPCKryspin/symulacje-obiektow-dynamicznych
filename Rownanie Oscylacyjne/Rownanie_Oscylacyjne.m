clear all;
close all;

Wn=[4, 9, 4, 3];
ksi=[0.9, 0.4, 0.4, 0.9];
u0=1;
du=1;
x0=u0;
x10=0;
stept=1;
kolor=['r','m','g','b'];

%Wn=[1.5];
%ksi=[-1.5];
%ksi=[-0.2];
%ksi=[0];
%ksi=[0.5];
%ksi=[1.5];

schemat='Rownanie_Oscylacyjne_1';
legenda='';

%{
figure
hold on;
grid on;
for i=1:4
    hold on
    Wn(i);
    ksi(i);
    [t]=sim(schemat);
    plot(t.tout,t.xwyj, kolor(i))
end
xlabel('t','interpreter','latex')
ylabel('h(t)','interpreter','latex')
figure(2)
%}

figure
hold on;
grid on;
for i=1:size(Wn,2)
    out=sim(schemat);
    plot(out.xwyj,kolor(i));
    %plot(real(x0),imag(x0),'o');
    legenda=[legenda,sprintf('ksi= %0.5g Wn= %0.5g,',ksi(i),Wn(i))];
end

    
legend(strsplit(legenda,','));
hold off;

dt=0.000001;
ui=0;
du=1000000;

schemat2='Rownanie_Oscylacyjne_2';
legenda2='';

%{
figure(2);
for j=1:4
    biegun1(j)=Wn(j)*((-ksi(j))+(ksi(j)^(2)-1)^(1/2));
    biegun2(j)=Wn(j)*((-ksi(j))-(ksi(j)^(2)-1)^(1/2));  
end

hold on
grid on
xlabel('Czesc rzeczywista','interpreter','latex')
ylabel('Czesc urojona','interpreter','latex')
plot(real(biegun1(1)),imag(biegun1(1)),'ro');
plot(real(biegun2(1)),imag(biegun2(1)),'ro');
plot(real(biegun1(2)),imag(biegun1(2)),'mo');
plot(real(biegun2(2)),imag(biegun2(2)),'mo');
plot(real(biegun1(3)),imag(biegun1(3)),'go');
plot(real(biegun2(3)),imag(biegun2(3)),'go');
plot(real(biegun1(4)),imag(biegun1(4)),'bo');
plot(real(biegun2(4)),imag(biegun2(4)),'bo');
%}

figure(2)
hold on;
grid on;
for i=1:size(Wn,2)
    out=sim(schemat2);
    plot(out.xwyj,kolor(i));
    %plot(real(x0),imag(x0),'o');
    legenda2=[legenda2,sprintf('ksi= %0.5g Wn= %0.5g,',ksi(i),Wn(i))];
end

legend(strsplit(legenda2,','));
hold off;
