clear all;
close all;
T_zewN=-20;
T_wewN=20;
T_pN=10;
Q_g=5000;
a=0.25;
K_cp=4;
K_cw=8;
K_cwp=a*K_cw;

T_zew = T_zewN-10:1:T_zewN+10;
T_p=(0.2*Q_g+T_zew*K_cw+T_zew*K_cw)/(0.2*K_cw+K_cp);
T_wew=(Q_g+K_cw*T_zew+0.25*T_p*K_cw)/(1.25*K_cw);


figure(1)
subplot(2,1,1);
hold on;
grid on;
plot(T_zew,T_wew,'b--');
plot(T_zewN,T_wewN,'ro');
title('T_{wew}(Tzew)');
subplot(2,1,2);
hold on;
grid on;
plot(T_zew,T_p,'b--');
plot(T_zewN,T_pN,'ro');
title('T_{p}(Tzew)');
